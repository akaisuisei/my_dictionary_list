#ifndef MY_DICTIONARY_H
# define MY_DICTIONARY_H

struct node
{
  char *key;
  void *value;
  struct node *next;
};

struct my_dictionary
{
  unsigned int size;
  struct node *head;
  struct node *tail;
};

struct my_dictionary *init_my_dictionary(void);
void add_elt(struct my_dictionary *dictionary, char *key, void *value);
void erase_key(struct my_dictionary *dictionary, char *key);
void delete_dictionary(struct my_dictionary *dictionary);
void *find_value(struct my_dictionary *dictionary, char *key);
int exist(struct my_dictionary *dictionary, char *key); 

#endif /*!MY_DICTIONARY_H */
