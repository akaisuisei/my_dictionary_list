CC=gcc
CFLAGS=-Wall -Werror -Werror -std=c99 -pedantic
SRC=$(wildcard *.c)
OBJ=$(SRC:.c=.o)

bin : $(OBJ)
	$(LINK.o) $^ $(LDLIDS) -o $@

clean :
	rm *.o
