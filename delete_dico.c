#include <stdlib.h>
#include "my_dictionary.h"
#include "my_string.h"
static void delete_node(struct node *node)
{
  free(node->value);
  free(node->key);
  free(node);
}

void erase_key(struct my_dictionary *dico, char *key)
{
  struct node *prev = NULL;
  struct node *current = dico->head;
  while (current != NULL)
  {
    if (is_equal(key, current->key))
    {
      prev->next = current->next;
      delete_node(current);
      dico->size--;
      return;
    }
    prev = current;
    current = current->next;
  }
}

void delete_dictionary(struct my_dictionary *dictionary)
{
  struct node *current = dictionary->head;
  while (current != NULL)
  {
    struct node *aux = current->next;
    delete_node(current);
    current = aux;
  }
}
