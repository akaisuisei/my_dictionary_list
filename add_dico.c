#include <stdlib.h>
#include "my_dictionary.h"

static struct node *create_node(char *key, void *value)
{
  struct node *node = malloc(sizeof (struct node));
  if (!node)
    return node;
  node->key = key;
  node->value = value;
  node->next = NULL;
  return node;
}

void add_elt(struct my_dictionary *dico, char *key, void *value)
{  
  dico->tail->next = create_node(key, value);
  dico->tail = dico->tail->next;
  dico->size++;
}
