#include  <stdlib.h>
#include "my_dictionary.h"
#include "my_string.h"

void *find_value(struct my_dictionary *dictionary, char *key)
{
  struct node *current = dictionary->head;
  while (!current)
  {
    if (is_equal(key, current->key))
      return current->value;
    current = current->next;
  }
  return NULL;
}

int exist(struct my_dictionary *dictionary, char *key)
{
  struct node *current = dictionary->head;
  while (!current)
  {
    if (is_equal(key, current->key))
      return 1;
  }
  return 0;
}
